#!/bin/sh

status='/var/lib/opkg/status'
package='enigma2-plugin-softcams-gosatplus-ncam'

if grep -q $package $status; then
echo "> removing package please wait..."
sleep 3
opkg remove $package

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#config
pack=gosatplus-ncam
version=15.2-r0
url="https://gitlab.com/eliesat/softcams/-/raw/main/mohamed-os/"
ipk="$pack-$version.ipk"
install="opkg install --force-reinstall"

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3

cd /tmp
set -e
wget -q --show-progress "$url/$ipk"
$install $ipk
set +e
cd ..
wait
rm -f /tmp/$ipk

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3
else
echo " installation failed"
fi

fi