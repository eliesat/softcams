#!/bin/sh

status='/var/lib/opkg/status'
package='enigma2-plugin-softcams-supcam-oscam'

if grep -q $package $status; then
echo "> removing package please wait..."
sleep 3
opkg remove $package

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#config
pack=supcam-oscam
version=11868-emu-r802
ipk="$pack-$version.ipk"
url="https://gitlab.com/eliesat/softcams/-/raw/main/mohamed-os/$ipk"

install="opkg install --force-reinstall"

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3

cd /tmp
set -e
wget -q --show-progress $url
$install $ipk
set +e
cd ..
wait
rm -f /tmp/$ipk

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3
else
echo " installation failed"
fi

fi