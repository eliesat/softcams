#!/bin/sh

echo "> Downloading & installing sofcam key file  Please Wait ..."
sleep 3s
dir=/etc/tuxbox/config

wget --show-progress -qO $dir/SoftCam.Key https://github.com/MOHAMED19OS/SoftCam_Emu/raw/main/SoftCam.Key


if [ -d $dir/oscamicam ]; then
cp $dir/SoftCam.Key $dir/oscamicam/
fi

echo "> installation of sofcam key file  finished"
sleep 3s
