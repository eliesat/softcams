#!/bin/sh

echo "> Downloading & installing sofcam key file  Please Wait ..."
sleep 3s
dir=/etc/tuxbox/config

wget -O /etc/tuxbox/config/SoftCam.Key http://www.softcam.org/deneme6.php?file=SoftCam.Key

if [ -d $dir/oscamicam ]; then
cp $dir/SoftCam.Key $dir/oscamicam/
fi

echo "> installation of sofcam key file  finished"
sleep 3s

exit 0