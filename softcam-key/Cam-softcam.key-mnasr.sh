#!/bin/sh

echo "> Downloading & installing softcam key file  Please Wait ..."
sleep 3
dir="/etc/tuxbox/config"

if [ -f "/usr/keys/SoftCam.Key" ]; then
   rm -rf "/usr/keys/SoftCam.Key" > /dev/null 2>&1
fi

if [ -f "/etc/tuxbox/config/SoftCam.Key" ]; then
   rm -rf "/etc/tuxbox/config/SoftCam.Key" > /dev/null 2>&1
fi

wget --show-progress -qO $dir/SoftCam.Key http://raw.githubusercontent.com/popking159/SoftCam/master/SoftCam.Key

if [ -d $dir/oscamicam ]; then
cp $dir/SoftCam.Key $dir/oscamicam/
fi

for i in ncam.dvbapi oscam.dvbapi
do
cat <<EOF >> $dir/$i
A: ::0A8F:0025 2600:000000:1FFF # CINEMA1
A: ::0A90:0022 2600:000000:1FFF # CINEMA2
A: ::0A8D:002B 2600:000000:1FFF # ALYAWM
A: ::0A8E:0028 2600:000000:1FFF # ALSAFWA
A: ::0A92:0034 2600:000000:1FFF # SERIES
A: ::0A91:0037 2600:000000:1FFF # SERIES 2
A: ::0A93:0031 2600:000000:1FFF # FANN
A: ::0A94:002E 2600:000000:1FFF # MUSIC NOW
EOF
done

echo "> installation of softcam key file  finished"
sleep 3
