#!/bin/sh

status='/var/lib/opkg/status'
package='enigma2-plugin-softcams-support'

if grep -q $package $status; then
opkg remove $package


else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#config
pack=support
version=2-r0.2
url="https://gitlab.com/eliesat/softcams/-/raw/main/support"
ipk="$pack-$version.ipk"
install="opkg install"

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3s

cd /tmp
set -e
wget --show-progress "$url/$ipk"
$install $ipk
set +e
cd ..
wait
rm -f /tmp/$ipk

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3s
else
echo " installation failed"
fi

fi
exit 0